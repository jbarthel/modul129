
# Dokumentation Labor 4 - Aggregierte statische Routen

 - Datum: 04.02.2022
 - Name: Justin Barthel
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](img/Labor4.png)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden.

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.13.1/24 interface=ether3
/ip/address add address=192.168.255.1/30 interface=ether1
/ip/address add address=192.168.136.1/24 interface=ether2

/ip/route/ add dst-address=192.168.128.0/17 gateway=192.168.255.2
```

## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R2
/ip/address add address=192.168.255.2/30 interface=ether1
/ip/address add address=192.168.131.1/24 interface=ether2
/ip/address add address=192.168.255.5/30 interface=ether4

/ip/route/ add dst-address=192.168.195.0/24 gateway=192.168.255.6
/ip/route/ add dst-address=192.168.23.0/24 gateway=192.168.255.1
/ip/route/ add dst-address=192.168.13.0/24 gateway=192.168.255.1
```

## Config R3
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R3
/ip/address add address=192.168.255.6/30 interface=ether4
/ip/address add address=192.168.195.1/24 interface=ether2

/ip/route/ add dst-address=192.168.0.0/16 gateway=192.168.255.5
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.13.2 255.255.255.0 192.168.13.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [GATEWAY]
ip 192.168.131.2 255.255.255.0 192.168.131.1
```

## Config VPC 3
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC3
# [IP] [MASKE] [GATEWAY]
ip 192.168.195.2 255.255.255.0 192.168.195.1
```

## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.0.0 mask 255.255.0.0 192.168.23.136
```

## Neue Lerninhalte
 - Aggregierte Routen

## Reflexion
