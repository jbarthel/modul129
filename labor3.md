# Dokumentation Labor 3 - Ping über Router (3 Subnetze) und lokalem PC

 - Datum: 04.02.2022
 - Name: Justin Barthel
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](img/Labor3.png)

## Cloud
 - br0 192.168.23.0
 - Eigener PC ist via OpenVPN (Layer2) mit br0 verbunden.

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 3 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.13.1/24 interface=ether3
/ip/address add address=192.168.255.1/30 interface=ether1

/ip/route add dst-address=192.168.130.0/24 gateway=192.168.255.2
```


## Config R2
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 2 Interfaces Enabled
```
/system/identity set name=R2
/ip/address add address=192.168.130.1/24 interface=ether2
/ip/address add address=192.168.255.2/30 interface=ether1

/ip/route add dst-address=192.168.23.0/24 gateway=192.168.255.1
/ip/route add dst-address=192.168.13.0/24 gateway=192.168.255.1
```


## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.13.2 255.255.255.0 192.168.13.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [GATEWAY]
ip 192.168.130.2 255.255.255.0 192.168.130.1
```


## Config Eigener Laptop
In *cmd.exe* als Admin:
```cmd
route add 192.168.13.0 mask 255.255.255.0 192.168.23.133
route add 192.168.130.0 mask 255.255.255.0 192.168.23.133
route add 192.168.130.0 mask 255.255.255.0 192.168.255.2
```


## Reflexion
Ich fand diese Aufgabe ganz okay. Anfangs hatte ich schwierigkeiten, Tim Schefer hat mir dann aber unter die Arme Gegriffen. Er hat mir das ganze erklärt und ich konnte dann das Labor 3 Komplett abschliessen.
