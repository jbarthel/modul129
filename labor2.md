
# Dokumentation Labor2 - Ping mit Router

 - Datum: 04.02.2022
 - Name: Justin Barthel
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)

![GNS3 Screenshot meines Labors](img/Labor2.png)

## Config R1
 - [MikroTik CHR 7.1rc6](https://mikrotik.com/download)
 - [GNS3 MikroTik Cloud Hosted Router](https://gns3.com/marketplace/appliances/mikrotik-cloud-hosted-router)
 - 2 Interfaces Enabled
```
/system/identity set name=R1
/ip/address add address=192.168.1.1/24 interface=ether1
/ip/address add address=192.168.2.1/24 interface=ether2
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [GATEWAY]
ip 192.168.1.2 255.255.255.0 192.168.1.1
```

## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [GATEWAY]
ip 192.168.2.2 255.255.255.0 192.168.2.1
```

## Neue Lerninhalte
 - MikroTik RouterOS war für mich komplett neue (Keine Erfahrungen)
 - Mehrere Interfaces auf Router konfigurieren
 - Erfahrungen zur Bedienung von GNS3 habe ich bereits in der ersten Übung gesammelt. Neu war das ganze Routing

## Reflexion
Ich hatte am Anfang Schwierigkeiten mit dem Router da ich noch garkeine Erfahrung hatte mit den Commands.
Danach hat mir Tim geholfen dies zu verstehen und nacher habe ich dann den Auftrag fertigstellen können.
